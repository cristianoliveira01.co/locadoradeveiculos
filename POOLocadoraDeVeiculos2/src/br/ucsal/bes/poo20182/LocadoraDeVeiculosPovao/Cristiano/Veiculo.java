package  br.ucsal.bes.poo20182.LocadoraDeVeiculosPovao.Cristiano;

public class Veiculo {
	private String placa;
	private Integer anoFabricacao;
	private Enum<Tipo> tipo;
	private Double valor;

	public Veiculo(String placa, Integer anoFabricacao, Enum< br.ucsal.bes.poo20182.LocadoraDeVeiculosPovao.Cristiano.Tipo> tipo) {

		this.placa = placa;
		this.anoFabricacao = anoFabricacao;
		this.tipo = tipo;

	}

	public Enum<Tipo> getTipo() {
		if (tipo.equals(Tipo.B�sico)) {
			this.valor = 100.45;
		} else if (this.tipo.equals(Tipo.Intermediario)) {
			this.valor = 130.10;
		} else {
			this.valor = 156.00;
		}
		return tipo;
	}

	public void setTipo(Enum<Tipo> tipo) {
		this.tipo = tipo;
	}

	public String getPlaca() {
		return placa;
	}

	public Integer getAnoFabricacao() {
		return anoFabricacao;
	}

	public Double getValor() {
		return valor;
	}

}
